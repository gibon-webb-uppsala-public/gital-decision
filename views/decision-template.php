<?php
/**
 * The template for the decision block
 *
 * @package Gital Decision
 */

// Defining settings.
$settings                    = array();
$settings['rewind_question'] = ( defined( 'DISABLE_REWIND_QUESTION' ) && DISABLE_REWIND_QUESTION ) ? false : true;

// Defining content.
$content                        = array();
$content['additional-classes']  = ( isset( $block['className'] ) ? ' ' . $block['className'] : '' );
$content['additional-classes'] .= ( $settings['rewind_question'] ) ? '' : ' g-decision--no-rewind-question';
$content['start-node-id']       = get_field( 'start_node' );

$content['start-node'] = array(
	'title'    => get_field( 'title', $content['start-node-id'] ),
	'question' => get_field( 'question', $content['start-node-id'] ),
	'answers'  => array(),
);

if ( get_field( 'answers', $content['start-node-id'] ) ) {
	$content['start-node']['answers'] = get_field( 'answers', $content['start-node-id'] );
}

$script = 'window.addEventListener("load", () => g_decision_setup("g-decisions--' . $content['start-node-id'] . '") )';
wp_add_inline_script( 'gital_decision_script', $script );
?>
<div id="<?php echo esc_attr( 'g-decisions--' . $content['start-node-id'] ); ?>" class="g-block g-decisions<?php echo gital_library\g_clean_classes( $content['additional-classes'], true ); ?>">
	<?php echo gital_decision\Components::decision( $content['start-node'], $settings['rewind_question'] ); ?>
</div>
