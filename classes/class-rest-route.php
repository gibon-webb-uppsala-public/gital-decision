<?php
/**
 * Rest Route
 *
 * @package Gital Decision
 */

namespace gital_decision;

if ( ! class_exists( 'Rest_Route' ) ) {
	/**
	 * Rest Route
	 *
	 * Registrers the REST route
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.0
	 * @since 1.0.0
	 */
	class Rest_Route {
		public function __construct() {
			add_action( 'rest_api_init', array( $this, 'decision_rest_route' ) );
		}

		/**
		 * Get decision
		 *
		 * @param array $payload The data of the decision to get.
		 *
		 * @return string The HTML of the decision.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function get_decision( $payload ) {
			$parameters = $payload->get_params();

			$id       = $parameters['id'] ?? false;
			$language = $parameters['language'] ?? false;

			if ( ! empty( $language ) ) {
				$id = apply_filters( 'wpml_object_id', $id, 'decisions', true, $language );
			}

			$content         = array(
				'title'    => get_field( 'title', $id ),
				'question' => get_field( 'question', $id ),
				'answers'  => array(),
			);
			$rewind_question = ( defined( 'DISABLE_REWIND_QUESTION' ) && DISABLE_REWIND_QUESTION ) ? false : true;

			if ( get_field( 'answers', $id ) ) {
				$content['answers'] = get_field( 'answers', $id );
			}

			return Components::decision( $content, $rewind_question );
		}

		/**
		 * Seting up the rest route
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 2.0.0
		 */
		public function decision_rest_route() {
			register_rest_route(
				'gital',
				'/decision/get_decision',
				array(
					'methods'             => 'POST',
					'callback'            => array( $this, 'get_decision' ),
					'permission_callback' => '__return_true',
				)
			);
		}
	}
}
