<?php
/**
 * CPT
 *
 * @package Gital Decision
 */

namespace gital_decision;

if ( ! class_exists( 'CPT' ) ) {
	/**
	 * CPT
	 *
	 * Adds and customizes the custom post typ decsions
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 1.0.0
	 */
	class CPT {
		public function __construct() {
			add_action( 'init', array( $this, 'create_decision_post_type' ) );
			add_filter( 'enter_title_here', array( $this, 'title_placeholder' ), 20, 2 );
			add_action( 'acf/init', array( $this, 'acf_fields' ) );
		}

		/**
		 * Create decision post type
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function create_decision_post_type() {
			register_post_type(
				'decisions',
				array(
					'labels'        => array(
						'name'                  => _x( 'Decision', 'Post type general name', 'gital-decision' ),
						'singular_name'         => _x( 'Decision', 'Post type singular name', 'gital-decision' ),
						'menu_name'             => _x( 'Decision tree', 'Admin Menu text', 'gital-decision' ),
						'name_admin_bar'        => _x( 'Decision', 'Add New on Toolbar', 'gital-decision' ),
						'add_new'               => __( 'Add new', 'gital-decision' ),
						'add_new_item'          => __( 'Add new decision', 'gital-decision' ),
						'new_item'              => __( 'New decision', 'gital-decision' ),
						'edit_item'             => __( 'Edit decision', 'gital-decision' ),
						'view_item'             => __( 'Show decision', 'gital-decision' ),
						'view_items'            => __( 'Show decision', 'gital-decision' ),
						'all_items'             => __( 'All decision', 'gital-decision' ),
						'search_items'          => __( 'Search decisions', 'gital-decision' ),
						'parent_item_colon'     => __( 'Parent decision:', 'gital-decision' ),
						'not_found'             => __( 'No decisions found.', 'gital-decision' ),
						'not_found_in_trash'    => __( 'No decisions found in trash.', 'gital-decision' ),
						'archives'              => _x( 'Decisions archive', 'The post type archive label used in nav menus. Default “Post Archives”.', 'gital-decision' ),
						'insert_into_item'      => _x( 'Add to decisions', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post).', 'gital-decision' ),
						'uploaded_to_this_item' => _x( 'Upload to this decision', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post).', 'gital-decision' ),
						'filter_items_list'     => _x( 'Filter the decisions list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”.', 'gital-decision' ),
						'items_list_navigation' => _x( 'Decisions list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”.', 'gital-decision' ),
						'items_list'            => _x( 'Decisions list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”.', 'gital-decision' ),
					),

					'description'   => __( 'Holds our decision specific data', 'gital-decision' ),
					'public'        => false,
					'show_ui'       => true,
					'show_in_menu'  => true,
					'has_archive'   => false,
					'menu_icon'     => 'dashicons-networking',
					'menu_position' => 5,
					'show_in_rest'  => false,
					'supports'      => array( 'title', 'page-attributes' ),
					'hierarchical'  => true,
					'taxonomies'    => array( '' ),
					'rewrite'       => array( 'slug' => __( 'decision', 'gital-decision' ) ),
				)
			);
		}

		/**
		 * Title placeholder
		 *
		 * @param string $title The current title placeholder.
		 * @param object $post The post to be edited.
		 *
		 * @return string The title placeholder.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function title_placeholder( $title, $post ) {
			if ( 'decision' === $post->post_type ) {
				$title = __( 'Decision name', 'gital-decision' );
				return $title;
			}

			return $title;
		}

		/**
		 * ACF Fields
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function acf_fields() {
			acf_add_local_field_group(
				array(
					'key'                   => 'group_61f3a8196da3a',
					'title'                 => __( 'Decision', 'gital-decision' ),
					'fields'                => array(
						array(
							'key'                 => 'field_626a456086c9f',
							'label'               => '',
							'name'                => '',
							'type'                => 'message',
							'instructions'        => '',
							'required'            => 0,
							'conditional_logic'   => 0,
							'wrapper'             => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'wpml_cf_preferences' => 3,
							'message'             => __( 'The name of the decision is never shown public and is only meant for internal use', 'gital-decision' ),
							'new_lines'           => 'wpautop',
							'esc_html'            => 0,
						),
						array(
							'key'                 => 'field_626a44d886c9e',
							'label'               => 'Beslutsrubrik',
							'name'                => 'title',
							'type'                => 'text',
							'instructions'        => __( 'Leave blank to not show a title for the question', 'gital-decision' ),
							'required'            => 0,
							'conditional_logic'   => 0,
							'wrapper'             => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'wpml_cf_preferences' => 3,
							'default_value'       => '',
							'placeholder'         => '',
							'prepend'             => '',
							'append'              => '',
							'maxlength'           => '',
						),
						array(
							'key'                 => 'field_61f3a8259c83f',
							'label'               => '',
							'name'                => 'question',
							'type'                => 'wysiwyg',
							'instructions'        => '',
							'required'            => 0,
							'conditional_logic'   => 0,
							'wrapper'             => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'default_value'       => '',
							'tabs'                => 'visual',
							'toolbar'             => 'decision',
							'media_upload'        => 0,
							'delay'               => 0,
							'wpml_cf_preferences' => 3,
						),
						array(
							'key'                 => 'field_61f3a87e9c840',
							'label'               => __( 'Answer', 'gital-decision' ),
							'name'                => 'answers',
							'type'                => 'repeater',
							'instructions'        => '',
							'required'            => 0,
							'conditional_logic'   => 0,
							'wrapper'             => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'collapsed'           => 'field_61f3a8969c841',
							'min'                 => 0,
							'max'                 => 5,
							'layout'              => 'table',
							'button_label'        => __( 'Add answer', 'gital-decision' ),
							'wpml_cf_preferences' => 3,
							'sub_fields'          => array(
								array(
									'key'                 => 'field_61f3a8969c841',
									'label'               => __( 'Answer', 'gital-decision' ),
									'name'                => 'answer',
									'type'                => 'text',
									'instructions'        => '',
									'required'            => 0,
									'conditional_logic'   => 0,
									'wrapper'             => array(
										'width' => '',
										'class' => '',
										'id'    => '',
									),
									'wpml_cf_preferences' => 3,
									'default_value'       => '',
									'placeholder'         => '',
									'prepend'             => '',
									'append'              => '',
									'maxlength'           => '',
								),
								array(
									'key'                 => 'field_61f3b10b53f65',
									'label'               => __( 'Answer description', 'gital-decision' ),
									'name'                => 'description',
									'type'                => 'textarea',
									'instructions'        => '',
									'required'            => 0,
									'conditional_logic'   => 0,
									'wrapper'             => array(
										'width' => '',
										'class' => '',
										'id'    => '',
									),
									'wpml_cf_preferences' => 3,
									'default_value'       => '',
									'placeholder'         => '',
									'maxlength'           => '',
									'rows'                => '',
									'new_lines'           => 'wpautop',
								),
								array(
									'key'                 => 'field_61f3a8c29c842',
									'label'               => __( 'Path', 'gital-decision' ),
									'name'                => 'road',
									'type'                => 'post_object',
									'instructions'        => '',
									'required'            => 0,
									'conditional_logic'   => 0,
									'wrapper'             => array(
										'width' => '',
										'class' => '',
										'id'    => '',
									),
									'post_type'           => array(
										0 => 'decisions',
									),
									'taxonomy'            => '',
									'allow_null'          => 0,
									'multiple'            => 0,
									'return_format'       => 'id',
									'wpml_cf_preferences' => 3,
									'ui'                  => 1,
								),
							),
						),
					),
					'location'              => array(
						array(
							array(
								'param'    => 'post_type',
								'operator' => '==',
								'value'    => 'decisions',
							),
						),
					),
					'menu_order'            => 0,
					'position'              => 'normal',
					'style'                 => 'default',
					'label_placement'       => 'top',
					'instruction_placement' => 'label',
					'hide_on_screen'        => '',
					'active'                => true,
					'description'           => '',
					'show_in_rest'          => 1,
				)
			);
		}
	}
}
