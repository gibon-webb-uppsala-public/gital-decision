<?php
/**
 * Block Registration
 *
 * @package Gital Decision
 */

namespace gital_decision;

if ( ! class_exists( 'Block_Registration' ) ) {
	/**
	 * Block Registration
	 *
	 * Adds the Decison block
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 1.0.0
	 */
	class Block_Registration {
		public function __construct() {
			add_action( 'acf/init', array( $this, 'block_decision' ) );
			add_action( 'acf/init', array( $this, 'acf_fields' ) );
			add_action( 'acf/fields/wysiwyg/toolbars', array( $this, 'add_additional_toolbars' ) );
		}

		/**
		 * Add additional toolbars
		 *
		 * @param array $toolbars The current array of toolbars.
		 *
		 * @return array The modified array of toolbars.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function add_additional_toolbars( $toolbars ) {
			$toolbars['Decision']    = array();
			$toolbars['Decision'][1] = array( 'formatselect', 'bold', 'italic', 'underline', 'undo', 'redo', 'link', 'bullist', 'numlist' );
			return $toolbars;
		}

		/**
		 * Block decision
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function block_decision() {
			if ( function_exists( 'acf_register_block_type' ) ) {
				acf_register_block_type(
					array(
						'name'            => 'gital-decision',
						'title'           => __( 'Decision tree', 'gital-decision' ),
						'description'     => __( 'Decision tree that fetches the decisions built', 'gital-decision' ),
						'render_template' => plugin_dir_path( __DIR__ ) . 'views/decision-template.php',
						'category'        => 'gital-decision',
						'icon'            => 'networking',
						'mode'            => 'edit',
						'align'           => 'full',
						'supports'        => array(
							'align' => false,
							'mode'  => false,
						),
					)
				);
			}
		}

		public function acf_fields() {
			acf_add_local_field_group(
				array(
					'key'                   => 'group_61f3ac7c05d56',
					'title'                 => __( 'Decision tree', 'gital-decision' ),
					'fields'                => array(
						array(
							'key'                 => 'field_61f3ac7c1f5b3',
							'label'               => __( 'Decision tree', 'gital-decision' ),
							'name'                => '',
							'type'                => 'message',
							'instructions'        => '',
							'required'            => 0,
							'conditional_logic'   => 0,
							'wrapper'             => array(
								'width' => '',
								'class' => 'g-editor__title',
								'id'    => '',
							),
							'wpml_cf_preferences' => 1,
							'message'             => '',
							'new_lines'           => 'wpautop',
							'esc_html'            => 0,
						),
						array(
							'key'                 => 'field_61f3ac9717ef0',
							'label'               => __( 'Startnode', 'gital-decision' ),
							'name'                => 'start_node',
							'type'                => 'post_object',
							'instructions'        => '',
							'required'            => 0,
							'conditional_logic'   => 0,
							'wrapper'             => array(
								'width' => '',
								'class' => '',
								'id'    => '',
							),
							'post_type'           => array(
								0 => 'decisions',
							),
							'taxonomy'            => '',
							'allow_null'          => 0,
							'multiple'            => 0,
							'return_format'       => 'id',
							'wpml_cf_preferences' => 1,
							'ui'                  => 1,
						),
					),
					'location'              => array(
						array(
							array(
								'param'    => 'block',
								'operator' => '==',
								'value'    => 'acf/gital-decision',
							),
						),
					),
					'menu_order'            => 0,
					'position'              => 'normal',
					'style'                 => 'default',
					'label_placement'       => 'top',
					'instruction_placement' => 'label',
					'hide_on_screen'        => '',
					'active'                => true,
					'description'           => '',
					'show_in_rest'          => 0,
					'modified'              => 1643359463,
				)
			);
		}
	}
}
