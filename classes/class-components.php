<?php
/**
 * Components
 *
 * @package Gital Google Services
 */

namespace gital_decision;

if ( ! class_exists( 'Components' ) ) {
	/**
	 * Components
	 *
	 * A collection of components
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 * @version 1.0.0
	 */
	class Components {
		/**
		 * Decision
		 *
		 * @param array $content The content of the decision.
		 * @param bool $rewind_question Set to true if the decision should promt if rewind is requested.
		 *
		 * @return string The HTML of the decision
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public static function decision( $content, $rewind_question ) {
			$component  = '<div class="g-decision g-decisions__item">';
			$component .= ! empty( $content['title'] ) ? '<h3 class="g-decision__title">' . $content['title'] . '</h3>' : '';
			$component .= '<div class="g-decision__question">' . $content['question'] . '</div>';

			if ( ! empty( $content['answers'] ) ) {
				$component .= '<div class="g-decision__answers">';

				foreach ( $content['answers'] as $answer ) {
					$component .= '<div class="g-decision__answer">';
					$component .= '<button class="g-decision__answer-button g-button g-button--white" data-answernode="' . $answer['road'] . '">' . $answer['answer'] . '</button>';
					if ( $answer['description'] ) {
						$component .= '<div class="g-decision__description">' . $answer['description'] . '</div>';
					}
					$component .= '</div>';
				}

				$component .= '</div>';
			}

			$component .= '<div class="g-decision__placeholder"><h3></h3><div></div></div>';
			$component .= '<div class="g-decision__back">';
			$component .= '<div class="g-decision__back-arrow">';
			$component .= \gital_library\g_icon( 'arrows/arrow_up.svg' );
			$component .= '</div>';
			if ( $rewind_question ) {
				$component .= '<div class="g-decision__back-question">';
				$component .= '<h3>' . __( 'Rewind', 'gital-decision' ) . '</h3>';
				$component .= '<p>' . __( "Are you sure that you'd like to rewind?", 'gital-decision' ) . '</p>';
				$component .= '<button class="g-decision__back-question--cancel g-button g-button--blank">' . __( 'Cancel', 'gital-decision' ) . '</button>';
				$component .= '<button class="g-decision__back-question--rewind g-button g-button--dark">' . __( 'Confirm', 'gital-decision' ) . '</button>';
				$component .= '</div>';
			}
			$component .= '</div>';
			$component .= '</div>';

			return $component;
		}
	}
}
