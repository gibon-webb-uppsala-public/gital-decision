#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Gital Decision\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-05 07:45+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.6.1; wp-5.9.3\n"
"X-Domain: gital-decision"

#: classes/class-cpt.php:188
msgid "Add answer"
msgstr ""

#: classes/class-cpt.php:44
msgid "Add new"
msgstr ""

#: classes/class-cpt.php:45
msgid "Add new decision"
msgstr ""

#: classes/class-cpt.php:43
msgctxt "Add New on Toolbar"
msgid "Decision"
msgstr ""

#: classes/class-cpt.php:42
msgctxt "Admin Menu text"
msgid "Decision tree"
msgstr ""

#: classes/class-cpt.php:50
msgid "All decision"
msgstr ""

#: classes/class-cpt.php:173 classes/class-cpt.php:193
msgid "Answer"
msgstr ""

#: classes/class-cpt.php:213
msgid "Answer description"
msgstr ""

#: classes/class-components.php:60
msgid "Are you sure that you'd like to rewind?"
msgstr ""

#: classes/class-components.php:61
msgid "Cancel"
msgstr ""

#: classes/class-components.php:62
msgid "Confirm"
msgstr ""

#: classes/class-cpt.php:111 classes/class-cpt.php:166
#: classes/class-block-registration.php:40
#: classes/class-block-registration.php:41
msgid "Decision"
msgstr ""

#: classes/class-cpt.php:74
msgid "decision"
msgstr ""

#: classes/class-cpt.php:93
msgid "Decision name"
msgstr ""

#: classes/class-block-registration.php:57
#: classes/class-block-registration.php:77
#: classes/class-block-registration.php:81
msgid "Decision tree"
msgstr ""

#: classes/class-block-registration.php:58
msgid "Decision tree that fetches the decisions built"
msgstr ""

#: classes/class-cpt.php:47
msgid "Edit decision"
msgstr ""

#. Author of the plugin
msgid "Gibon Webb"
msgstr ""

#. Name of the plugin
msgid "Gital Decision"
msgstr ""

#: classes/class-cpt.php:63
msgid "Holds our decision specific data"
msgstr ""

#. Author URI of the plugin
msgid "https://gibon.se/"
msgstr ""

#: classes/class-cpt.php:136
msgid "Leave blank to not show a title for the question"
msgstr ""

#: classes/class-cpt.php:46
msgid "New decision"
msgstr ""

#: classes/class-cpt.php:54
msgid "No decisions found in trash."
msgstr ""

#: classes/class-cpt.php:53
msgid "No decisions found."
msgstr ""

#: classes/class-cpt.php:56
msgctxt ""
"Overrides the “Insert into post”/”Insert into page” phrase (used when "
"inserting media into a post)."
msgid "Add to decisions"
msgstr ""

#: classes/class-cpt.php:57
msgctxt ""
"Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used "
"when viewing media attached to a post)."
msgid "Upload to this decision"
msgstr ""

#: classes/class-cpt.php:52
msgid "Parent decision:"
msgstr ""

#: classes/class-cpt.php:233
msgid "Path"
msgstr ""

#: classes/class-cpt.php:40
msgctxt "Post type general name"
msgid "Decision"
msgstr ""

#: classes/class-cpt.php:41
msgctxt "Post type singular name"
msgid "Decision"
msgstr ""

#: classes/class-components.php:59
msgid "Rewind"
msgstr ""

#: classes/class-cpt.php:58
msgctxt ""
"Screen reader text for the filter links heading on the post type listing "
"screen. Default “Filter posts list”/”Filter pages list”."
msgid "Filter the decisions list"
msgstr ""

#: classes/class-cpt.php:60
msgctxt ""
"Screen reader text for the items list heading on the post type listing "
"screen. Default “Posts list”/”Pages list”."
msgid "Decisions list"
msgstr ""

#: classes/class-cpt.php:59
msgctxt ""
"Screen reader text for the pagination heading on the post type listing "
"screen. Default “Posts list navigation”/”Pages list navigation”."
msgid "Decisions list navigation"
msgstr ""

#: classes/class-cpt.php:51
msgid "Search decisions"
msgstr ""

#: classes/class-cpt.php:48 classes/class-cpt.php:49
msgid "Show decision"
msgstr ""

#: classes/class-block-registration.php:99
msgid "Startnode"
msgstr ""

#. Description of the plugin
msgid ""
"The Gital Decision is made with passion in Uppsala, Sweden. If you'd like "
"support, please contact us at webb@gibon.se."
msgstr ""

#: classes/class-cpt.php:127
msgid ""
"The name of the decision is never shown public and is only meant for "
"internal use"
msgstr ""

#: classes/class-cpt.php:55
msgctxt ""
"The post type archive label used in nav menus. Default “Post Archives”."
msgid "Decisions archive"
msgstr ""
