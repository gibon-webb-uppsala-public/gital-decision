��    %      D      l      l     m     �     �     �     �     �     �  '   �                    '     5  .   C     r      �  0   �     �     �     �  |     �   �           1     6      V     w  �   ~  �   "  �   �     U     f  	   t  Q   ~  ]   �     .  �  7     �	     �	     �	     
     
     $
     )
  $   :
     _
  	   f
     p
     w
     �
  &   �
     �
     �
  4   �
       "        7     J     ^     t     �     �     �     �     �     �     �     �     �     �  S   �     P     ]   Add New on ToolbarDecision Add new Add new decision Admin Menu textDecision tree All decision Answer Answer description Are you sure that you'd like to rewind? Cancel Confirm Decision Decision name Decision tree Decision tree that fetches the decisions built Edit decision Holds our decision specific data Leave blank to not show a title for the question New decision No decisions found in trash. No decisions found. Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post).Add to decisions Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post).Upload to this decision Parent decision: Path Post type general nameDecision Post type singular nameDecision Rewind Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”.Filter the decisions list Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”.Decisions list Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”.Decisions list navigation Search decisions Show decision Startnode The name of the decision is never shown public and is only meant for internal use The post type archive label used in nav menus. Default “Post Archives”.Decisions archive decision Project-Id-Version: Gital Decision
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2022-05-05 09:35+0000
PO-Revision-Date: 2023-01-05 07:57+0000
Last-Translator: 
Language-Team: Svenska
Language: sv_SE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.6.1; wp-5.9.3
X-Domain: gital-decision Beslut Lägg till ny Lägg till nytt beslut Beslutsträd Alla beslut Svar Svarsbeskrivning Är du säker på att du vill backa? Avbryt Bekräfta Beslut Beslutsnamn Beslutsträd Beslutsträd som hämtar byggda beslut Redigera beslut Beslutsdata Lämna blank för att inte visa en titel på frågan Nytt beslut Inga beslut funna i papperskorgen. Inga beslut funna. Lägg till i beslut Ladda upp till beslut Föräldrabeslut: Väg Beslut Beslut Backa Filtrera beslutslistan Beslutslistan Beslutslistenavigation Sök beslut Visa beslut Startnod Namnet på beslutet visas aldrig utåt utan används endast internt i adminpanelen. Beslutsarkiv beslut 