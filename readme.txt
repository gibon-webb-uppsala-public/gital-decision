=== Gital Decision ===
Contributors: gibonadmin
Requires at least: 5.0
Tested up to: 5.6
Requires PHP: 7.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Gital Decision is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb@gibon.se.

== Description ==
The Gital Decision is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb@gibon.se.

== Changelog ==

= 1.9.0 - 2024.10.21 =
* The plugin is now moved to the new repo.

= 1.8.0 - 2024.09.04 =
* The plugin now utilizes the new repo.

= 1.7.0 - 2023.03.29 = 
* Update: Gibon Webb Uppsala is now Gibon Webb.

= 1.6.1 - 2023.02.21 = 
* Update: Delayed the back function.
* Update: Disabled the back function arrow on mobile.

= 1.5.0 - 2023.01.05 = 
* Update: Added comments.
* Update: Changed the REST route to work with POST instead of GET and added the language argument if the decision tree is used together with WPML.
* Update: Added more translations.

= 1.4.1 - 2022.10.17 = 
* Update: Changed the sass variables to the new semantic.

= 1.2.0 - 2022.09.06 = 
* Update: Added WPML support.

= 1.2.0 - 2022.08.25 = 
* Bugfix: Minor bugfixes.

= 1.1.1 - 2022.05.12 = 
* Bugfix: Compiled the scss.

= 1.1.1 - 2022.05.12 = 
* Bugfix: Hierarchical did not work.
* Bugfix: Some spelling mistakes fixed.
* Bugfix: z-index was wrong on the back dialog.

= 1.1.0 - 2022.05.05 = 
* Update: Added translations.

= 1.0.3 - 2022.05.05 = 
* Update: Init.
