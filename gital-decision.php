<?php
/**
 * Plugin Name: Gital Decision
 * Author: Gibon Webb
 * Version: 1.9.0
 *
 * Author URI: https://gibon.se/
 * Description: The Gital Decision is made with passion in Uppsala, Sweden. If you'd like support, please contact us at webb@gibon.se.
 *
 * @package Gital Decision
 */

namespace gital_decision;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Init the updater.
// Source: https://github.com/YahnisElsts/plugin-update-checker.
require plugin_dir_path( __FILE__ ) . 'vendor/yahnis-elsts/plugin-update-checker/plugin-update-checker.php';
$updateChecker = \Puc_v4_Factory::buildUpdateChecker(
	'https://packages.gital.se/wordpress/gital-decision.json',
	__FILE__,
	'gital-decision'
);

/**
 * Load textdomain and languages
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
function textdomain() {
	load_plugin_textdomain( 'gital-decision', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'gital_decision\textdomain' );

// Constants.
if ( ! defined( 'G_BOI_ROOT' ) ) {
	define( 'G_BOI_ROOT', plugins_url( '', __FILE__ ) );
}
if ( ! defined( 'G_BOI_ASSETS' ) ) {
	define( 'G_BOI_ASSETS', G_BOI_ROOT . '/assets' );
}
if ( ! defined( 'G_BOI_RESOURSES' ) ) {
	define( 'G_BOI_RESOURSES', G_BOI_ROOT . '/assets/resources' );
}
if ( ! defined( 'G_BOI_ROOT_PATH' ) ) {
	define( 'G_BOI_ROOT_PATH', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'G_BOI_ASSETS_PATH' ) ) {
	define( 'G_BOI_ASSETS_PATH', G_BOI_ROOT_PATH . 'assets/' );
}
if ( ! defined( 'G_BOI_FUNCTIONS_PATH' ) ) {
	define( 'G_BOI_FUNCTIONS_PATH', G_BOI_ROOT_PATH . 'functions/' );
}
if ( ! defined( 'G_BOI_CLASSES_PATH' ) ) {
	define( 'G_BOI_CLASSES_PATH', G_BOI_ROOT_PATH . 'classes/' );
}
if ( ! defined( 'G_BOI_VIEWS_PATH' ) ) {
	define( 'G_BOI_VIEWS_PATH', G_BOI_ROOT_PATH . 'views/' );
}

/**
 * Enquene public scripts
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 */
function assets() {
	wp_register_style( 'gital_decision_style', G_BOI_ASSETS . '/styles/gital.decision.min.css', array(), '1.1.0' );
	wp_enqueue_style( 'gital_decision_style' );

	wp_register_script( 'gital_decision_script', G_BOI_ASSETS . '/scripts/gital.decision.min.js', array(), '1.1.0', true );
	wp_enqueue_script( 'gital_decision_script' );
}
add_action( 'wp_enqueue_scripts', 'gital_decision\assets' );

// Components.
require_once plugin_dir_path( __FILE__ ) . 'classes/class-components.php';

// Custom post types.
require_once plugin_dir_path( __FILE__ ) . 'classes/class-cpt.php';
new CPT();

// Block registration.
require_once plugin_dir_path( __FILE__ ) . 'classes/class-block-registration.php';
new Block_Registration();

// Rest route.
require_once plugin_dir_path( __FILE__ ) . 'classes/class-rest-route.php';
new Rest_Route();
