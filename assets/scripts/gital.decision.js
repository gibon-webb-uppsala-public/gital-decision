/**
 * Get decision
 *
 * @param {number} id The id of the decision to get
 * @return {string} The HTML of the decision
 */
const get_decision = async ( id ) => {
	// eslint-disable-next-line no-undef
	const current_language = g_get_cookie( 'wp-wpml_current_language' );

	const response = await fetch( '/wp-json/gital/decision/get_decision', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify( {
			id: id,
			language: current_language,
		} ),
	} );
	return response.json();
};

/**
 * Rewind tree to the desired decision
 *
 * @param {Object} decision The decision to rewind to
 */
const rewind = ( decision ) => {
	decision.classList.remove( 'g-decision--answered' );
	const answers = decision.querySelectorAll( '.g-decision__answer--answered' );
	const decision_children = decision.parentElement.querySelectorAll( '.g-decision' );
	let delete_element = false;
	answers.forEach( ( answer ) => {
		answer.classList.remove( 'g-decision__answer--answered' );
		const button = answer.querySelector( '.g-button--primary' );
		if ( button ) {
			button.classList.remove( 'g-button--primary' );
			button.classList.add( 'g-button--white' );
		}
	} );
	decision_children.forEach( child => {
		if ( delete_element ) {
			child.remove();
		}
		if ( child === decision ) {
			delete_element = true;
		}
	} );
};

/**
 * Handles the answer
 *
 * @param {Object} decision      The decision to handle
 * @param {Object} decision_tree The current decision tree
 */
const answered_handler = ( decision, decision_tree ) => {
	const back_element = decision.querySelector( '.g-decision__back' );
	if ( ! decision_tree.classList.contains( 'g-decision--no-rewind-question' ) ) {
		const cancel_button = back_element.querySelector( '.g-decision__back-question--cancel' );
		const rewind_button = back_element.querySelector( '.g-decision__back-question--rewind' );

		back_element.onclick = ( element ) => {
			if ( ! element.target.classList.contains( 'g-button' ) ) {
				if ( ! decision.classList.contains( 'g-decision--confirm' ) ) {
					decision.classList.add( 'g-decision--confirm' );
				} else {
					decision.classList.remove( 'g-decision--confirm' );
				}
			}
		};
		cancel_button.onclick = () => {
			if ( decision.classList.contains( 'g-decision--confirm' ) ) {
				decision.classList.remove( 'g-decision--confirm' );
			}
		};
		rewind_button.onclick = () => {
			if ( decision.classList.contains( 'g-decision--confirm' ) ) {
				decision.classList.remove( 'g-decision--confirm' );
				rewind( decision );
			}
		};
	} else {
		back_element.onclick = () => rewind( decision );
	}
};

/**
 * The answer click handler
 *
 * @param {Object} answer        The clicked answer
 * @param {Object} decision_tree The current decision tree
 */
const answer_click_handler = ( answer, decision_tree ) => {
	const decision = answer.parentElement.parentElement.parentElement;
	const decision_id = decision.parentElement.id;
	if ( ! decision.classList.contains( 'g-decision--answered' ) ) {
		answer.parentElement.classList.add( 'g-decision__answer--answered' );
		answer.classList.add( 'g-button--primary' );
		answer.classList.remove( 'g-button--white' );
		decision.classList.add( 'g-decision--answered', 'g-decision--loading' );
		const answer_node = answer.dataset.answernode;

		setTimeout( () => {
			decision.classList.add( 'g-decision--enable-back' );
		}, 1000 );

		get_decision( answer_node )
			.then( ( response ) => {
				decision.classList.remove( 'g-decision--loading' );
				decision.insertAdjacentHTML( 'afterend', response );
				decision_setup( decision_id );
				answered_handler( decision, decision_tree );
			} );
	}
};

/**
 * Init the decision system
 *
 * @param {number} id The id of the decision tree
 */
const decision_setup = ( id ) => {
	const decision_tree = document.getElementById( id );
	const answers = decision_tree.querySelectorAll( '.g-decision:not(.g-decision--answered) .g-decision__answer-button' );
	if ( answers.length >= 2 ) {
		answers.forEach( ( answer ) => {
			answer.addEventListener( 'click', () => answer_click_handler( answer, decision_tree ) );
		} );
	}
};
window.g_decision_setup = decision_setup;
